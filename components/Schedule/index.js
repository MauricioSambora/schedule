import { Container, Box, Button } from "@chakra-ui/react";

import { firebaseClient } from "../../config/firebase/client";

export const Schedule = () => {
    const logout = ()=> firebaseClient.auth().signOut()
    return (
        <Container p={4} centerContent>
            <Box  >
                <div>
                    <Button onClick={logout}>Sair</Button>

                </div>
            </Box>
        </Container>
    )
}
